from django.urls import path
from common.views import (SnakeView, HomeView, SignUpView, AnyaHubLoginView, LoginSuccessView, SignupSuccessView, LogoutView)

urlpatterns = [
    path('snakes/', SnakeView.get, name='snakes'),
    path("", HomeView.get, name="index"),
    path("signup/", SignUpView.as_view(), name="signup"),
    path("signup/index/", SignupSuccessView.as_view()),
    path('login/', AnyaHubLoginView.as_view(), name='login'),
    path("login/index/", LoginSuccessView.as_view()),
    path("logout/", LogoutView.as_view, name='logout'),
]