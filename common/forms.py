from crispy_bootstrap5.bootstrap5 import FloatingField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, UsernameField
from django.forms import forms, PasswordInput


class AnyaHubSignUpForm(UserCreationForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()


class AnyaHubChangePasswordForm(PasswordChangeForm):
    helper = FormHelper()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()


class AnyaHubLoginForm(forms.Form):
    username = forms.Field(widget=UsernameField)
    password = forms.Field(widget=PasswordInput)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div(

            FloatingField('username', css_class="form-control",placeholder="username"),
                FloatingField('password', css_class="form-control", placeholder="password"),
                    css_class="form-floating mb-3",
            )
        )
        )



