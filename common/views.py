import json as simplejson

from django.contrib.auth.forms import UserCreationForm
from django.views.generic import TemplateView
from django_registration.backends.activation.views import ActivationView, RegistrationView

from anyahub import settings
from common.forms import AnyaHubLoginForm
from django.shortcuts import render
from django.views import View, generic
from django.core.paginator import Paginator
import requests
from django.contrib.auth.views import LoginView


class SnakeView(View):

    def get(request):
        url = 'https://nas.er.usgs.gov/api/v2/occurrence/search?group=Reptiles-Snakes'
        raw = requests.get(url)
        json = simplejson.loads(raw.text)
        paginator = Paginator(json['results'], per_page=25)
        page_number = request.GET.get("page")
        page_obj = paginator.get_page(page_number)
        return render(request, 'snakes.html', {"data": page_obj})


class HomeView(View):
    def get(request):
        current_user = request.user
        user_logged_in = False
        if current_user.is_authenticated:
            user_logged_in = True
        context = {'current_user':current_user, "user_logged_in":user_logged_in}
        return render(request, 'index.html', context=context)


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    template_name = "registration/signup.html"
    success_url = settings.SIGNUP_REDIRECT_URL


class AnyaHubLoginView(LoginView):
    form_class = AnyaHubLoginForm
    template_name = 'registration/login.html'
    success_url = settings.LOGIN_REDIRECT_URL


class LoginSuccessView(TemplateView):
    template_name = 'login_success.html'


class SignupSuccessView(TemplateView):
    template_name = 'signup_success.html'

class LogoutView(TemplateView):
    template_name = 'registration/logout_success.html'






#