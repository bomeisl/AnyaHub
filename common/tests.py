from django.test import TestCase
from django.urls import reverse

class StaticPageFetchTest(TestCase):

    def url_resolve_helper(self, uri):
        path = reverse(uri)
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200)

    def test_successfully_fetch_home_page(self):
        self.url_resolve_helper('index')

    def test_successfully_fetch_snakes(self):
        self.url_resolve_helper('snakes')
