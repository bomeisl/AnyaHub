<h1 align="center"> AnyaHub </h1>
<hr/>

<p align="center">An archive, journal, scheduler, and router for your social media posts</p> 

<div align="center">

[![Build Status](https://app.travis-ci.com/bomeisl/AnyaHub.svg?branch=main)](https://travis-ci.org/joemccann/dillinger)
[![N|Solid](https://www.python.org/static/community_logos/python-powered-w-70x28.png)](https://www.python.org)
[![N|Solid](https://www.djangoproject.com/m/img/badges/djangomade124x25_grey.gif)](https://www.djangoproject.com)
[![Docker CI](https://github.com/bomeisl/AnyaHub/actions/workflows/docker-ci.yml/badge.svg)](https://github.com/bomeisl/AnyaHub/actions/workflows/docker-ci.yml)

</div>

<h3 align="center">
Development Updates
</h3>
<hr/>

<div align="center">
<p align="center"> 1/20/2023</p>
<b> Containerization </b>

<p>
AnyaHub is made up of many different services. So far we have SQL databases, a Django application, a static storage bucket,
microservices, and an SMS router. Each of them will be running on different hardware, a different OS, and different environments.
The database will run 
on Azure SQL, the Django application will be hosted on a Virtual Machine with Google App Engine, the static storage bucket
is hosted by CloudFlare, the microservices will be hosted by Google Cloud Run, and of course the SMS router
will be built and maintained by me. You can see how managing the setup of each service and juggling all of them
will be messy. Plus testing will be computationally expensive and unreliable. And verifying that the entire system
works in concert as a whole (integration testing) will be a nightmare. Containerization allows us to create OS-agnostic
builds (will work on any OS, properly configured). A Docker container can be set up and configured very quickly and cheaply 
to simulate the correct hardware/OS/environment stack for any sort of automated tests that need to run. Furthermore,
containerization allows us to produce builds that are hardware, OS, and environment agnostic. These containerized builds
allow us the freedom to use any kind of hardware and OS we like.
</p>

<p>For example, in the image below Docker is running a container for our development web server and development database server:</p>

![alt text](containerization.png?raw=true)

</div>

<div align="center">
<p align="center"> 1/18/2023</p>
<b> Building a more professional CI/CD setup </b>
<p> 
Now that AnyaHub is entering a more complex stage of development, 
a little DevOps infrastructure setup now will save us a ton of headaches later.
Previously all local development code was committed directly to the <code>main branch</code> with only 
minimal testing and checks. The correct approach is to maintain a 'development branch' (<code>develop branch</code>) that I will commit new code to from my local machine.
Whenever I commit new code, it will run through a Continuous Integration (CI) pipeline. This will verify that my new code does not break the
existing code or the functionality of existing features. When I and the automated tests and checks run by the CI pipeline
are satisfied that the new code is stable, I have the option of using <code>git merge</code> to add the new code in the development branch 
into the <code>main branch</code> and it will then become part of the app.
</p>

<p align="center">
For example, I wrote some tests that visit all pages of AnyaHub and verify that a 200 OK HTTP code was returned (the page rendered successfully). 
If I commit some code that breaks something, the CI will see those checks failing and give me a notice so I can revert the commit or commit another 
patch to fix it.
</p>

<p align="center"> 
Looking forward, the <code>main branch</code> will be the 'production branch' holding the stable code staged for deployment
to the live app at <a href="www.anyahub.com">www.anyahub.com</a>. The app is not live yet, but once the first stable build
is released, it will be at that link. This is the Continuous Deployment (CD) part.
</p>
</div>


<h2 align="center"> Mastodon Server </h2>

AnyaHub now generates a personalized Mastodon server, application, and account under the hood which is fully integrated with
the AnyaHub RSS feed. Mastodon is an open source decentralized version of Twitter with millions of users.
If you host your own server - which this app does for you - you get to make your own rules. 100% free speech.
Also, no ads, custom feed algorithm, etc. And posts on your Mastodon server can automatically be cross-posted to your Twitter
account via Twitter's API

<h2 align="center"> Feature Checklist</h2>

- [x] User Interface and Authentication Flow
- [x] Snake Advisory System
- [ ] Centralized RSS feed to organize social media post queue
- [ ] Twitter API integration
- [ ] Facebook API integration
- [ ] Instragram API integration
- [ ] Write an automated test suite so Django CI pipeline works 

> [!CAUTION]
> Certain snakes can fly. The Snake Advisory database is not exhaustive