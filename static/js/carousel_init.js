const myCarouselElement = document.querySelector('#apolloCarousel')

const carousel = new bootstrap.Carousel(myCarouselElement, {
  interval: 2000,
  touch: false
})