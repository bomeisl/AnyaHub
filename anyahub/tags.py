from django import template

register = template.Library()

@register.simple_tag(name="current_user")
def get_current_user(user):
    pass