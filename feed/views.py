from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from feed.models import Post


class PostListView(LoginRequiredMixin, ListView):
    model = Post
    paginate_by = 30
    template_name = 'feed.html'

