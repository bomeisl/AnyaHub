# Generated by Django 4.2.9 on 2024-01-15 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0002_post_date_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='MastodonApp',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_name', models.TextField(default='feed', max_length=100)),
                ('api_base_url', models.TextField(default='https://mastodon.social', max_length=150)),
                ('scopes', models.CharField(choices=[('read', 'Read'), ('write', 'Write'), ('follow', 'Follow'), ('push', 'Push')])),
                ('to_file', models.TextField(default='anyahub_client_credentials.secret', max_length=120)),
                ('client_id', models.TextField(default=None, max_length=400)),
                ('client_secret', models.TextField(default=None, max_length=400)),
                ('access_token', models.TextField(default=None, max_length=400)),
            ],
        ),
    ]
