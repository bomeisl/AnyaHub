from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now


class Feed(models.Model):
    title = models.CharField(default='', max_length=200)
    description = models.CharField(default='', max_length=400)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=now)


class Post(models.Model):
    title = models.CharField(default='', max_length=200)
    body = models.CharField(default='', max_length=300)
    feed = models.ForeignKey(to=Feed, on_delete=models.CASCADE, default=None)
    timestamp = models.DateTimeField(default=now)
