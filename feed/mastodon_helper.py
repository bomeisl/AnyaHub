"""
Created by kbomeisl
1/15/24 12:21 PM
"""

from collections import defaultdict
from dataclasses import dataclass
from models import MastodonApp
from mastodon import Mastodon

@dataclass
class MastodonUser:
    username: str = ''
    password: str = ''


@dataclass
class MastodonApp:
    username: str = ''
    password: str = ''
    client_id: str = ''
    client_secret: str = ''

class MastodonHelper:
    user = MastodonUser()
    app = MastodonApp()
    api_instance = None

    def get_user_credentials(self, email, password):
        self.user.username = email
        self.user.password = password

    def create_app(self):
        client_credentials = Mastodon.create_app(
            client_name=self.user.username,
            scopes=["read","write","follow","push"],
            redirect_uris= None, #TODO(user authentication success screen)
            website=None,
            api_base_url='https://mastodon.social',
            request_timeout=300,
            to_file=self.user.username+'_clientcred.secret',
            user_agent='feed'
        )
        self.user.client_id, self.user.client_secret = client_credentials
        return client_credentials


    def init_api(self):
        self.api_instance = Mastodon(
            client_id= self.app.client_id,
            client_secret= self.app.client_secret,
            version_check_mode="created"
        )

    def log_in_user(self):
        self.api_instance.log_in(
            username=self.user.username,
            password=self.user.password,
            to_file=self.user.username+'_access_token.secret'
        )

    def get_app_credentials(self):
        description = self.api_instance.account_verify_credentials().source.note




