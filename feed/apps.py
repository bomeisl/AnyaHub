from django.apps import AppConfig


class AnyahubConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'feed'
